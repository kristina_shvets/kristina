import { showModal } from "./modal";
import { keyDown } from "../arena";
export function showWinnerModal(fighter) {
  // call showModal function 
  document.removeEventListener("keydown",keyDown);
  const source = fighter.source;
  const name = fighter.name;
  showModal({title:name,bodyElement:source});
}
