import { controls } from '../../constants/controls';


export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    
    if(firstFighter.health <=0){  //secondFigter win
      const healthBarEl = document.getElementById(`left-fighter-indicator`);
      healthBarEl.style.width = `0%`;
      resolve(secondFighter);
    }
    if(secondFighter.health<=0){   //firstFigter win
      const healthBarEl = document.getElementById(`right-fighter-indicator`);
      healthBarEl.style.width = `0%`;
      
      resolve(firstFighter);
    }
  });
}
export function getDamage(attacker, defender,position,mainHealthVal) {
  if(defender.isInBlock){  //if defender is in block;
    console.log(`Игрок  ${defender.name} находится в защите`)
    return 0;
  } 
  if(attacker.isInBlock){  //if attacker is in block;
    console.log(`Игрок ${attacker.name} в защите,не может атаковать`);
    return 0;
  } 
  if(defender.defence >= attacker.attack) return 0  //if defender defence more than attacker.attack return 0
  const damage = getHitPower(attacker)-getBlockPower(defender);  //Our damage
  if(damage<0) return 0;                                
  changeHealthBar(position,defender,mainHealthVal,damage); //change Defender Bar of Health
  return damage; 
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random()* (2 - 1) + 1;
  return fighter.attack*criticalHitChance;
}


export function getBlockPower(fighter) {
  const dodgeChance = Math.random()* (2 - 1) + 1;
  return fighter.defense * dodgeChance;
}


export function getComboAttack(attacker){
    const damage = 2*attacker.attack;
    return damage
}

export function changeHealthBar(position,defender,mainHealthVal,damage){
  const healthBarEl = document.getElementById(`${position}-fighter-indicator`);
  const percents = ((defender.health-damage)*100)/mainHealthVal;
  healthBarEl.style.width = `${percents}%`;
  
}